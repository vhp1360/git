<div dir=rtl align=right><h1>بنام خدا</h1></div>

# Git
here you find notes regarding _git_

## Config
there are three types *Config* Files.
1. _/etc/gitconfig_: this config file for whole system.to _read_ or _write_ this file you should use **--system** option.
2. _~/./gitconfig or ~/.config/git/config_: this is user config file and should **--global** option to _read_ and _write_ to this file.
3. _.git/config_: this config file stay in Directory which contain Repository.
first configs you should do:
```bash
    git config --global user.name "Your Complete Name"
    git config --global user.email yourMail@Domain
    git config --list <-- to check the configs
```
## Start git
### init
first initialize current directory into Git Repository.
```bash
    git init
```

### Adding files
after initialize we need add our files to repo. for this `add` command used:
```bash
    git add [FilePattern | * ]
    git reset  [FilePattern | * ]   <-- the second command used to remove file from git repo.
    git ls-files                    <-- to list which files are tracked
```

### goto Commit
```bash
    git log --oneline
    git checkout ID_comes_from_previous
```

## Git Stach
sometimes we need go to other branch without saving current changes, this time **Stach** is useful.
```bash
    git status
    git stash
    git status
```
you could have multiple staches, the name is like _stash@{0}_ .
some useful commands:
```bash
    git stash list
    git stash -u                                    <-- add untracked files
    git stash pop [name is optional]
    git stash show                                  <-- show diff
    git stash show -p                               <-- show complete diff
    git stash branch BranchName stashName           <-- create branch from stash
    git stash drop stashName
    git stash clear
```
